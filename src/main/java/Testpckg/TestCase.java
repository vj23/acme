package Testpckg;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class TestCase {

 public static void main(String[] args) {
	System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	ChromeDriver driver = new ChromeDriver();
	driver.manage().window().maximize();
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	driver.get("https://acme-test.uipath.com/account/login");
	
	
	driver.findElementById("email").clear();
	driver.findElementById("email").sendKeys("charlzjaison@gmail.com");
	driver.findElementById("password").clear();
	driver.findElementById("password").sendKeys("Xperia");
	driver.findElementById("buttonLogin").click();
	
	Actions builder=new Actions(driver);
	WebElement vendor = driver.findElementByXPath("(//button[text()=' Vendors'])");
	
//	WebElement search = driver.findElementByPartialLinkText("Search for Vendor");
	
	builder.moveToElement(vendor).perform();
	builder.moveToElement(driver.findElementByLinkText("Search for Vendor")).click().perform();
	
	driver.findElementById("vendorTaxID").clear();
	driver.findElementById("vendorTaxID").sendKeys("RO657483");
	driver.findElementById("buttonSearch").click();
//	driver.findElementByPartialLinkText("Search").click();
	
}
}