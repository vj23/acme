package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import bi.framework.design.ProjectMethodClass;

public class searchResults extends ProjectMethodClass {

	public searchResults()
	{
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	@FindBy(how = How.ID,using="buttonSearch") WebElement results;
	public searchResults finalResult()
	{
	click(results);
		
		return this;
	}
}
