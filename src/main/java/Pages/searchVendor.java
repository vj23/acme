package Pages;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import bi.framework.design.ProjectMethodClass;


public class searchVendor extends ProjectMethodClass {
	

	public searchVendor()
	{
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}

	@FindBy(how = How.ID,using="vendorTaxID") WebElement vendorID;
public searchVendor search()
{
	clearAndType(vendorID,"RO657483");
	return this;
	
	}
@FindBy(how = How.ID,using="buttonSearch") WebElement results;
public searchVendor finalResult()
{
click(results);
	
	return this;
}

}
