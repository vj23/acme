package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import bi.framework.design.ProjectMethodClass;

public class dashboard extends ProjectMethodClass {
	

	public dashboard()
	{
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how = How.XPATH,using="(//button[text()=' Vendors'])") WebElement vendor;
	@FindBy(how= How.LINK_TEXT,using=("Search for Vendor"))WebElement search;
	
public searchVendor mouseover()
{
	Actions builder=new Actions(driver);
	WebElement vendorss = vendor;
	builder.moveToElement(vendorss).perform();
	builder.moveToElement(search).click().perform();
	return new searchVendor();
	
}
	
}
