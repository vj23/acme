package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import bi.framework.design.ProjectMethodClass;

public class loginPg extends ProjectMethodClass {

	public loginPg() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	
    @FindBy(how = How.ID,using="email") WebElement eleUsername;
	@FindBy(how = How.ID,using="password") WebElement elePassword;
	@FindBy(how = How.ID,using="buttonLogin") WebElement eleLogin;

	public loginPg enterUsername() {
		
	clearAndType(eleUsername, "charlzjaison@gmail.com");
		
//		driver.findElementById("email").sendKeys("charlzjaison@gmail.com");
		return this; 
	}
	public loginPg enterPassword() {
		//WebElement elePassword = locateElement("id", "password");
//		clearAndType(elePassword, "Xperia");
		
		driver.findElementById("password").sendKeys("Xperia");
		return this;
	}
	public dashboard clickLogin() {
		//WebElement eleLogin = locateElement("class", "decorativeSubmit");
//	    click(eleLogin);
	    /*HomePage hp = new HomePage();
	    return hp;*/
		
		driver.findElementById("buttonLogin").click();
	    return new dashboard();
	}
}
	

